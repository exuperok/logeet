<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TrueController extends Controller
{
    public function indexAction()
    {
        $name = 'true app';
        return $this->render('ApiBundle:Default:index.html.twig', array('name' => $name));
    }

}
