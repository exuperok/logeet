What is Logeet?
===============

Logget is an Open-Source software providing market place for placing classified ads for selling and renting home annd appartment.
The software is built on Symfony 2.6, the PHP framework for building web applications and apis.


What's inside?
--------------

* On the homepage, the user sees the latest active classifieds
* A user can ask for all the homes in a given category or area
* A user can refins the list of displayed active classifieds  with some keywords
* A user clicks on a home classifieds to see detailed information
* A user can posts a home classified for rent or sale
* A user can apply to tale part in the partners programms
* A partner can retrieve the list of current active classifieds through a restfull api
* An admin can manage and moderate the web application
* An admin can manage and moderate the partners 

How it is built?
--------------



How to set it up?
------------------
